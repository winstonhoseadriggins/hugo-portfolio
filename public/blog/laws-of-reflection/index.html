<!DOCTYPE html>
<html lang="en-US">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="google-site-verification" content="d8FqOlyeTt2rtW43rgmHku52lictkYRKWRiBDMMUgYU" />
    <meta name="author" content="map[]" />
    <meta name="description" content="Development of Blockchain Database Applications. The beauty of the blockchain is its distributed architecture. Once A Transaction is verified by the blockchain, A Permanent Digital Signature Will Propogate Throughout Eternity. DW84 Inc LLC Foundation Special Agent primary role is the security and storage of client digital assets.">
    <link rel="shortcut icon" type="image/x-icon" href="https://winstonhoseadriggins.gitlab.io/hugo-portfolio/img/favicon.ico">
    <title>The Laws of Reflection</title>
    <meta name="generator" content="Hugo 0.21" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="https://winstonhoseadriggins.gitlab.io/hugo-portfolio/css/main.css" />
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css" />
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:200,400,200bold,400old" />
    
    <!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
			<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->

    
  </head>

  <body>
    <div id="wrap">

      
      <nav class="navbar navbar-default">
  <div class="container">
    <div class="navbar-header">
      <a class="navbar-brand" href="https://winstonhoseadriggins.gitlab.io/hugo-portfolio/"><i class="fa fa-home"></i></a>
    </div>
    <div id="navbar">
      <ul class="nav navbar-nav navbar-right">
      
        
        <li><a href="/hugo-portfolio/blog/">BLOG</a></li>
        
        <li><a href="/hugo-portfolio/projects/">PROJECTS</a></li>
        
      
      </ul>
    </div>
  </div>
</nav>

      
      <div class="container">
        <div class="blog-post">
          <h3>
            <strong><a href="https://winstonhoseadriggins.gitlab.io/hugo-portfolio/blog/laws-of-reflection/">The Laws of Reflection</a></strong>
          </h3>
        </div>
        <div class="blog-title">
          <h4>
          September 17, 2015
            &nbsp;&nbsp;
            
          </h4>
        </div>
        <div class="panel panel-default">
          <div class="panel-body">
            <div class="blogpost">
              <ul>
<li>Rob Pike</li>
</ul>

<p>Reflection in computing is the ability of a program to examine its own structure, particularly through types; it&rsquo;s a form of metaprogramming. It&rsquo;s also a great source of confusion.</p>

<p>In this article we attempt to clarify things by explaining how reflection works in Go. Each language&rsquo;s reflection model is different (and many languages don&rsquo;t support it at all), but this article is about Go, so for the rest of this article the word &ldquo;reflection&rdquo; should be taken to mean &ldquo;reflection in Go&rdquo;.</p>

<ul>
<li>Types and interfaces</li>
</ul>

<p>Because reflection builds on the type system, let&rsquo;s start with a refresher about types in Go.</p>

<p>Go is statically typed. Every variable has a static type, that is, exactly one type known and fixed at compile time: <code>int</code>, <code>float32</code>, <code>*MyType</code>, <code>[]byte</code>, and so on. If we declare</p>

<pre><code>type MyInt int

var i int
var j MyInt
</code></pre>

<p>then <code>i</code> has type <code>int</code> and <code>j</code> has type <code>MyInt</code>. The variables <code>i</code> and <code>j</code> have distinct static types and, although they have the same underlying type, they cannot be assigned to one another without a conversion.</p>

<p>One important category of type is interface types, which represent fixed sets of methods. An interface variable can store any concrete (non-interface) value as long as that value implements the interface&rsquo;s methods. A well-known pair of examples is <code>io.Reader</code> and <code>io.Writer</code>, the types <code>Reader</code> and <code>Writer</code> from the [[<a href="http://golang.org/pkg/io/][io">http://golang.org/pkg/io/][io</a> package]]:</p>

<pre><code>// Reader is the interface that wraps the basic Read method.
type Reader interface {
    Read(p []byte) (n int, err error)
}

// Writer is the interface that wraps the basic Write method.
type Writer interface {
    Write(p []byte) (n int, err error)
}
</code></pre>

<p>Any type that implements a <code>Read</code> (or <code>Write</code>) method with this signature is said to implement <code>io.Reader</code> (or <code>io.Writer</code>). For the purposes of this discussion, that means that a variable of type <code>io.Reader</code> can hold any value whose type has a <code>Read</code> method:</p>

<pre><code>    var r io.Reader
    r = os.Stdin
    r = bufio.NewReader(r)
    r = new(bytes.Buffer)
    // and so on
</code></pre>

<p>It&rsquo;s important to be clear that whatever concrete value <code>r</code> may hold, <code>r</code>&rsquo;s type is always <code>io.Reader</code>: Go is statically typed and the static type of <code>r</code> is <code>io.Reader</code>.</p>

<p>An extremely important example of an interface type is the empty interface:</p>

<pre><code>interface{}
</code></pre>

<p>It represents the empty set of methods and is satisfied by any value at all, since any value has zero or more methods.</p>

<p>Some people say that Go&rsquo;s interfaces are dynamically typed, but that is misleading. They are statically typed: a variable of interface type always has the same static type, and even though at run time the value stored in the interface variable may change type, that value will always satisfy the interface.</p>

<p>We need to be precise about all this because reflection and interfaces are closely related.</p>

<ul>
<li>The representation of an interface</li>
</ul>

<p>Russ Cox has written a [[<a href="http://research.swtch.com/2009/12/go-data-structures-interfaces.html][">http://research.swtch.com/2009/12/go-data-structures-interfaces.html][</a> detailed blog post]] about the representation of interface values in Go. It&rsquo;s not necessary to repeat the full story here, but a simplified summary is in order.</p>

<p>A variable of interface type stores a pair: the concrete value assigned to the variable, and that value&rsquo;s type descriptor. To be more precise, the value is the underlying concrete data item that implements the interface and the type describes the full type of that item. For instance, after</p>

<pre><code>    var r io.Reader
    tty, err := os.OpenFile(&quot;/dev/tty&quot;, os.O_RDWR, 0)
    if err != nil {
        return nil, err
    }
    r = tty
</code></pre>

<p><code>r</code> contains, schematically, the (value, type) pair, (<code>tty</code>, <code>*os.File</code>). Notice that the type <code>*os.File</code> implements methods other than <code>Read</code>; even though the interface value provides access only to the <code>Read</code> method, the value inside carries all the type information about that value. That&rsquo;s why we can do things like this:</p>

<pre><code>    var w io.Writer
    w = r.(io.Writer)
</code></pre>

<p>The expression in this assignment is a type assertion; what it asserts is that the item inside <code>r</code> also implements <code>io.Writer</code>, and so we can assign it to <code>w</code>. After the assignment, <code>w</code> will contain the pair (<code>tty</code>, <code>*os.File</code>). That&rsquo;s the same pair as was held in <code>r</code>. The static type of the interface determines what methods may be invoked with an interface variable, even though the concrete value inside may have a larger set of methods.</p>

<p>Continuing, we can do this:</p>

<pre><code>    var empty interface{}
    empty = w
</code></pre>

<p>and our empty interface value <code>empty</code> will again contain that same pair, (<code>tty</code>, <code>*os.File</code>). That&rsquo;s handy: an empty interface can hold any value and contains all the information we could ever need about that value.</p>

<p>(We don&rsquo;t need a type assertion here because it&rsquo;s known statically that <code>w</code> satisfies the empty interface. In the example where we moved a value from a <code>Reader</code> to a <code>Writer</code>, we needed to be explicit and use a type assertion because <code>Writer</code>&rsquo;s methods are not a subset of <code>Reader</code>&rsquo;s.)</p>

<p>One important detail is that the pair inside an interface always has the form (value, concrete type) and cannot have the form (value, interface type). Interfaces do not hold interface values.</p>

<p>Now we&rsquo;re ready to reflect.</p>

<ul>
<li><p>The first law of reflection</p></li>

<li><ol>
<li>Reflection goes from interface value to reflection object.</li>
</ol></li>
</ul>

<p>At the basic level, reflection is just a mechanism to examine the type and value pair stored inside an interface variable. To get started, there are two types we need to know about in [[<a href="http://golang.org/pkg/reflect/][package">http://golang.org/pkg/reflect/][package</a> reflect]]: [[<a href="http://golang.org/pkg/reflect/#Type][Type]">http://golang.org/pkg/reflect/#Type][Type]</a>] and [[<a href="http://golang.org/pkg/reflect/#Value][Value]">http://golang.org/pkg/reflect/#Value][Value]</a>]. Those two types give access to the contents of an interface variable, and two simple functions, called <code>reflect.TypeOf</code> and <code>reflect.ValueOf</code>, retrieve <code>reflect.Type</code> and <code>reflect.Value</code> pieces out of an interface value. (Also, from the <code>reflect.Value</code> it&rsquo;s easy to get to the <code>reflect.Type</code>, but let&rsquo;s keep the <code>Value</code> and <code>Type</code> concepts separate for now.)</p>

<p>Let&rsquo;s start with <code>TypeOf</code>:</p>

<pre><code>package main

import (
    &quot;fmt&quot;
    &quot;reflect&quot;
)

func main() {
    var x float64 = 3.4
    fmt.Println(&quot;type:&quot;, reflect.TypeOf(x))
}
</code></pre>

<p>This program prints</p>

<pre><code>type: float64
</code></pre>

<p>You might be wondering where the interface is here, since the program looks like it&rsquo;s passing the <code>float64</code> variable <code>x</code>, not an interface value, to <code>reflect.TypeOf</code>. But it&rsquo;s there; as [[<a href="http://golang.org/pkg/reflect/#TypeOf][godoc">http://golang.org/pkg/reflect/#TypeOf][godoc</a> reports]], the signature of <code>reflect.TypeOf</code> includes an empty interface:</p>

<pre><code>// TypeOf returns the reflection Type of the value in the interface{}.
func TypeOf(i interface{}) Type
</code></pre>

<p>When we call <code>reflect.TypeOf(x)</code>, <code>x</code> is first stored in an empty interface, which is then passed as the argument; <code>reflect.TypeOf</code> unpacks that empty interface to recover the type information.</p>

<p>The <code>reflect.ValueOf</code> function, of course, recovers the value (from here on we&rsquo;ll elide the boilerplate and focus just on the executable code):</p>

<pre><code>    var x float64 = 3.4
    fmt.Println(&quot;value:&quot;, reflect.ValueOf(x).String())
</code></pre>

<p>prints</p>

<pre><code>value: &lt;float64 Value&gt;
</code></pre>

<p>(We call the <code>String</code> method explicitly because by default the <code>fmt</code> package digs into a <code>reflect.Value</code> to show the concrete value inside.
The <code>String</code> method does not.)</p>

<p>Both <code>reflect.Type</code> and <code>reflect.Value</code> have lots of methods to let us examine and manipulate them. One important example is that <code>Value</code> has a <code>Type</code> method that returns the <code>Type</code> of a <code>reflect.Value</code>. Another is that both <code>Type</code> and <code>Value</code> have a <code>Kind</code> method that returns a constant indicating what sort of item is stored: <code>Uint</code>, <code>Float64</code>, <code>Slice</code>, and so on. Also methods on <code>Value</code> with names like <code>Int</code> and <code>Float</code> let us grab values (as <code>int64</code> and <code>float64</code>) stored inside:</p>

<pre><code>    var x float64 = 3.4
    v := reflect.ValueOf(x)
    fmt.Println(&quot;type:&quot;, v.Type())
    fmt.Println(&quot;kind is float64:&quot;, v.Kind() == reflect.Float64)
    fmt.Println(&quot;value:&quot;, v.Float())
</code></pre>

<p>prints</p>

<pre><code>type: float64
kind is float64: true
value: 3.4
</code></pre>

<p>There are also methods like <code>SetInt</code> and <code>SetFloat</code> but to use them we need to understand settability, the subject of the third law of reflection, discussed below.</p>

<p>The reflection library has a couple of properties worth singling out. First, to keep the API simple, the &ldquo;getter&rdquo; and &ldquo;setter&rdquo; methods of <code>Value</code> operate on the largest type that can hold the value: <code>int64</code> for all the signed integers, for instance. That is, the <code>Int</code> method of <code>Value</code> returns an <code>int64</code> and the <code>SetInt</code> value takes an <code>int64</code>; it may be necessary to convert to the actual type involved:</p>

<pre><code>    var x uint8 = 'x'
    v := reflect.ValueOf(x)
    fmt.Println(&quot;type:&quot;, v.Type())                            // uint8.
    fmt.Println(&quot;kind is uint8: &quot;, v.Kind() == reflect.Uint8) // true.
    x = uint8(v.Uint())                                       // v.Uint returns a uint64.
</code></pre>

<p>The second property is that the <code>Kind</code> of a reflection object describes the underlying type, not the static type. If a reflection object contains a value of a user-defined integer type, as in</p>

<pre><code>    type MyInt int
    var x MyInt = 7
    v := reflect.ValueOf(x)
</code></pre>

<p>the <code>Kind</code> of <code>v</code> is still <code>reflect.Int</code>, even though the static type of <code>x</code> is <code>MyInt</code>, not <code>int</code>. In other words, the <code>Kind</code> cannot discriminate an int from a <code>MyInt</code> even though the <code>Type</code> can.</p>

<ul>
<li><p>The second law of reflection</p></li>

<li><ol>
<li>Reflection goes from reflection object to interface value.</li>
</ol></li>
</ul>

<p>Like physical reflection, reflection in Go generates its own inverse.</p>

<p>Given a <code>reflect.Value</code> we can recover an interface value using the <code>Interface</code> method; in effect the method packs the type and value information back into an interface representation and returns the result:</p>

<pre><code>// Interface returns v's value as an interface{}.
func (v Value) Interface() interface{}
</code></pre>

<p>As a consequence we can say</p>

<pre><code>    y := v.Interface().(float64) // y will have type float64.
    fmt.Println(y)
</code></pre>

<p>to print the <code>float64</code> value represented by the reflection object <code>v</code>.</p>

<p>We can do even better, though. The arguments to <code>fmt.Println</code>, <code>fmt.Printf</code> and so on are all passed as empty interface values, which are then unpacked by the <code>fmt</code> package internally just as we have been doing in the previous examples. Therefore all it takes to print the contents of a <code>reflect.Value</code> correctly is to pass the result of the <code>Interface</code> method to the formatted print routine:</p>

<pre><code>    fmt.Println(v.Interface())
</code></pre>

<p>(Why not <code>fmt.Println(v)</code>? Because <code>v</code> is a <code>reflect.Value</code>; we want the concrete value it holds.) Since our value is a <code>float64</code>, we can even use a floating-point format if we want:</p>

<pre><code>    fmt.Printf(&quot;value is %7.1e\n&quot;, v.Interface())
</code></pre>

<p>and get in this case</p>

<pre><code>3.4e+00
</code></pre>

<p>Again, there&rsquo;s no need to type-assert the result of <code>v.Interface()</code> to <code>float64</code>; the empty interface value has the concrete value&rsquo;s type information inside and <code>Printf</code> will recover it.</p>

<p>In short, the <code>Interface</code> method is the inverse of the <code>ValueOf</code> function, except that its result is always of static type <code>interface{}</code>.</p>

<p>Reiterating: Reflection goes from interface values to reflection objects and back again.</p>

<ul>
<li><p>The third law of reflection</p></li>

<li><ol>
<li>To modify a reflection object, the value must be settable.</li>
</ol></li>
</ul>

<p>The third law is the most subtle and confusing, but it&rsquo;s easy enough to understand if we start from first principles.</p>

<p>Here is some code that does not work, but is worth studying.</p>

<pre><code>    var x float64 = 3.4
    v := reflect.ValueOf(x)
    v.SetFloat(7.1) // Error: will panic.
</code></pre>

<p>If you run this code, it will panic with the cryptic message</p>

<pre><code>panic: reflect.Value.SetFloat using unaddressable value
</code></pre>

<p>The problem is not that the value <code>7.1</code> is not addressable; it&rsquo;s that <code>v</code> is not settable. Settability is a property of a reflection <code>Value</code>, and not all reflection <code>Values</code> have it.</p>

<p>The <code>CanSet</code> method of <code>Value</code> reports the settability of a <code>Value</code>; in our case,</p>

<pre><code>    var x float64 = 3.4
    v := reflect.ValueOf(x)
    fmt.Println(&quot;settability of v:&quot;, v.CanSet())
</code></pre>

<p>prints</p>

<pre><code>settability of v: false
</code></pre>

<p>It is an error to call a <code>Set</code> method on an non-settable <code>Value</code>. But what is settability?</p>

<p>Settability is a bit like addressability, but stricter. It&rsquo;s the property that a reflection object can modify the actual storage that was used to create the reflection object. Settability is determined by whether the reflection object holds the original item. When we say</p>

<pre><code>    var x float64 = 3.4
    v := reflect.ValueOf(x)
</code></pre>

<p>we pass a copy of <code>x</code> to <code>reflect.ValueOf</code>, so the interface value created as the argument to <code>reflect.ValueOf</code> is a copy of <code>x</code>, not <code>x</code> itself. Thus, if the statement</p>

<pre><code>    v.SetFloat(7.1)
</code></pre>

<p>were allowed to succeed, it would not update <code>x</code>, even though <code>v</code> looks like it was created from <code>x</code>. Instead, it would update the copy of <code>x</code> stored inside the reflection value and <code>x</code> itself would be unaffected. That would be confusing and useless, so it is illegal, and settability is the property used to avoid this issue.</p>

<p>If this seems bizarre, it&rsquo;s not. It&rsquo;s actually a familiar situation in unusual garb. Think of passing <code>x</code> to a function:</p>

<pre><code>f(x)
</code></pre>

<p>We would not expect <code>f</code> to be able to modify <code>x</code> because we passed a copy of <code>x</code>&rsquo;s value, not <code>x</code> itself. If we want <code>f</code> to modify <code>x</code> directly we must pass our function the address of <code>x</code> (that is, a pointer to <code>x</code>):</p>

<pre><code>f(&amp;x)
</code></pre>

<p>This is straightforward and familiar, and reflection works the same way. If we want to modify <code>x</code> by reflection, we must give the reflection library a pointer to the value we want to modify.</p>

<p>Let&rsquo;s do that. First we initialize <code>x</code> as usual and then create a reflection value that points to it, called <code>p</code>.</p>

<pre><code>    var x float64 = 3.4
    p := reflect.ValueOf(&amp;x) // Note: take the address of x.
    fmt.Println(&quot;type of p:&quot;, p.Type())
    fmt.Println(&quot;settability of p:&quot;, p.CanSet())
</code></pre>

<p>The output so far is</p>

<pre><code>type of p: *float64
settability of p: false
</code></pre>

<p>The reflection object <code>p</code> isn&rsquo;t settable, but it&rsquo;s not <code>p</code> we want to set, it&rsquo;s (in effect) <code>*p</code>. To get to what <code>p</code> points to, we call the <code>Elem</code> method of <code>Value</code>, which indirects through the pointer, and save the result in a reflection <code>Value</code> called <code>v</code>:</p>

<pre><code>    v := p.Elem()
    fmt.Println(&quot;settability of v:&quot;, v.CanSet())
</code></pre>

<p>Now <code>v</code> is a settable reflection object, as the output demonstrates,</p>

<pre><code>settability of v: true
</code></pre>

<p>and since it represents <code>x</code>, we are finally able to use <code>v.SetFloat</code> to modify the value of <code>x</code>:</p>

<pre><code>    v.SetFloat(7.1)
    fmt.Println(v.Interface())
    fmt.Println(x)
</code></pre>

<p>The output, as expected, is</p>

<pre><code>7.1
7.1
</code></pre>

<p>Reflection can be hard to understand but it&rsquo;s doing exactly what the language does, albeit through reflection <code>Types</code> and <code>Values</code> that can disguise what&rsquo;s going on. Just keep in mind that reflection Values need the address of something in order to modify what they represent.</p>

<ul>
<li>Structs</li>
</ul>

<p>In our previous example <code>v</code> wasn&rsquo;t a pointer itself, it was just derived from one. A common way for this situation to arise is when using reflection to modify the fields of a structure. As long as we have the address of the structure, we can modify its fields.</p>

<p>Here&rsquo;s a simple example that analyzes a struct value, <code>t</code>. We create the reflection object with the address of the struct because we&rsquo;ll want to modify it later. Then we set <code>typeOfT</code> to its type and iterate over the fields using straightforward method calls (see [[<a href="http://golang.org/pkg/reflect/][package">http://golang.org/pkg/reflect/][package</a> reflect]] for details). Note that we extract the names of the fields from the struct type, but the fields themselves are regular <code>reflect.Value</code> objects.</p>

<pre><code>    type T struct {
        A int
        B string
    }
    t := T{23, &quot;skidoo&quot;}
    s := reflect.ValueOf(&amp;t).Elem()
    typeOfT := s.Type()
    for i := 0; i &lt; s.NumField(); i++ {
        f := s.Field(i)
        fmt.Printf(&quot;%d: %s %s = %v\n&quot;, i,
            typeOfT.Field(i).Name, f.Type(), f.Interface())
    }
</code></pre>

<p>The output of this program is</p>

<pre><code>0: A int = 23
1: B string = skidoo
</code></pre>

<p>There&rsquo;s one more point about settability introduced in passing here: the field names of <code>T</code> are upper case (exported) because only exported fields of a struct are settable.</p>

<p>Because <code>s</code> contains a settable reflection object, we can modify the fields of the structure.</p>

<pre><code>    s.Field(0).SetInt(77)
    s.Field(1).SetString(&quot;Sunset Strip&quot;)
    fmt.Println(&quot;t is now&quot;, t)
</code></pre>

<p>And here&rsquo;s the result:</p>

<pre><code>t is now {77 Sunset Strip}
</code></pre>

<p>If we modified the program so that <code>s</code> was created from <code>t</code>, not <code>&amp;t</code>, the calls to <code>SetInt</code> and <code>SetString</code> would fail as the fields of <code>t</code> would not be settable.</p>

<ul>
<li>Conclusion</li>
</ul>

<p>Here again are the laws of reflection:</p>

<ul>
<li><p>Reflection goes from interface value to reflection object.</p></li>

<li><p>Reflection goes from reflection object to interface value.</p></li>

<li><p>To modify a reflection object, the value must be settable.</p></li>
</ul>

<p>Once you understand these laws reflection in Go becomes much easier to use, although it remains subtle. It&rsquo;s a powerful tool that should be used with care and avoided unless strictly necessary.</p>

<p>There&rsquo;s plenty more to reflection that we haven&rsquo;t covered — sending and receiving on channels, allocating memory, using slices and maps, calling methods and functions — but this post is long enough. We&rsquo;ll cover some of those topics in a later article.</p>

              <hr>
              <div class="related-posts">
                <h5>Related Posts</h5>
                
              </div>
            </div>
          </div>
          <hr>
        <div class="disqus">
  <div id="disqus_thread"></div>
  <script type="text/javascript">

    (function() {
      
      
      if (window.location.hostname == "localhost")
        return;

      var disqus_shortname = '';
      var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
      dsq.src = '//' + disqus_shortname + '.disqus.com/embed.js';
      (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
    })();
  </script>
  <noscript>Please enable JavaScript to view the <a href="http://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
  <a href="http://disqus.com/" class="dsq-brlink">comments powered by <span class="logo-disqus">Disqus</span></a>
</div>
        </div>
      </div>
      
    </div>

    
    <footer>
  <div id="footer">
    <div class="container">
      <p class="text-muted">&copy; All rights reserved. Powered by <a href="https://winstonhoseadriggins.github.io/hugo-mainroad/">DW84 Inc LLC Propogation Agent</a> and
      <a href="https://winstonhoseadriggins.github.io/hugo-boot4/">DW84 Inc LLC Foundation</a> with ♥</p>
    </div>
  </div>
</footer>
<div class="footer"></div>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
<script src="https://winstonhoseadriggins.gitlab.io/hugo-portfolio/js/docs.min.js"></script>
<script src="https://winstonhoseadriggins.gitlab.io/hugo-portfolio/js/main.js"></script>

<script src="https://winstonhoseadriggins.gitlab.io/hugo-portfolio/js/ie10-viewport-bug-workaround.js"></script>


    
  </body>
</html>
